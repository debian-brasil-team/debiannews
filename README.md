# DebianNews

Nosso objetivo é ter um grupo de pessoas para:

- Escrever e publicar notícias sobre o Debian no Brasil
- Traduzir e publicar mensagens para listas ou notícias internacionais que sejam interessantes para nós brasileiros.

# Meios para divulgar

## Site Debian Brasil

- Endereço: <https://debianbrasil.org.br>
- Repositório: <https://gitlab.com/DebianBrasil/debianbrasil.org.br>

## Canal no telegram Debian Brasil Notícias

- Endereço: <https://t.me/DebianBrasilNoticias>
- Atualmente apenas Paulo e Daniel tem permissão para mandar mensagens.

## Lista debian-news-portuguese

- Endereço: <debian-news-portuguese@lists.debian.org>
- Histório: <https://lists.debian.org/debian-news-portuguese>
- Essa lista é moderada, por isso atualmente apenas Paulo e Daniel tem permissão para mandar mensagens para ela.

## Outras listas

- Listas do Debian em português: <https://wiki.debian.org/Brasil/Listas>
- Algumas notícias podem ser enviadas para outras listas além da debian-news-portuguese.

# Notícias publicadas

- [2023](https://salsa.debian.org/debian-brasil-team/debiannews/-/blob/main/2023/publicadas-2023.md)