## Debian Brasil na FOSDEM'23

O DD Paulo Santana (phls), da equipe de tradução do Debian para o português do Brasil,
está coordenando a [Translations DevRoom](https://fosdem.org/2023/schedule/track/translations/)
da [FOSDEM'23](https://fosdem.org/2023), que está acontecendo entre os dias 4 e 5
de fevereiro de 2023.

A FOSDEM é um dos maiores eventos voltados ao software livre da Europa, e acontece
anualmente sempre na primeira semana de fevereiro, em Bruxelas, Bélgica. As
apresentações são [transmitidas on-line](https://fosdem.org/2023/schedule/).

Nesta sala, domingo dia 5, também haverá a
[apresentação do DD Jean-Philippe Mengual (texou)](https://fosdem.org/2023/schedule/event/translations_building_an_atractive_way_in_an_old_infra_for_new_translators/)
sobre sua iniciativa de orientar novos(as) tradutores(as) no Projeto Debian
a partir de experiências da equipe francesa.
