# Notícias publicadas em 2023.


* Debian Brasil no FOSDEM'23:
    * [Lista debian-news-portuguese](https://lists.debian.org/debian-news-portuguese/2023/msg00000.html)

* Oficina de tradução Debian para iniciantes
    * [Lista debian-news-portuguese](https://lists.debian.org/debian-news-portuguese/2023/msg00001.html)
    * [Lista debian-user-portuguese](https://lists.debian.org/debian-user-portuguese/2023/02/msg00000.html)
    * [Canal Debian Brasil Notícias no Telegram](https://t.me/DebianBrasilNoticias)
    * [Perfil Debian Brasil no Twitter](https://twitter.com/debianbrasil/status/1625239667665772544)