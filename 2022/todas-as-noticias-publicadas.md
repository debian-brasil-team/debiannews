# Notícias publicadas em 2021 e 2022

## Participação da comunidade Debian no Latinoware e na Campus Party Brasil em 2022

Após um longo período de ausência, estamos trazendo novamente notícias sobre o Debian :-)
A comunidade Debian estará presente em dois importantes eventos que acontecerão em novembro de 2022.

De 2 a 4 de novembro acontecerá o Latinoware 2022 - 19º Congresso Latino-americano de Software Livre e Tecnologias Abertas, no Hotel Golden Park Internacional em Foz do Iguaçu.
A organização do Latinoware gentilmente cedeu um estande para a comunidade Debian, por isso convidamos você a participar do evento também e visitar o nosso espaço.

As inscrições já estão abertas, e a chamada de palestras está aberta até o dia 31 de agosto.
Para mais informações sobre o Latinoware, acesse:
https://latinoware.org

De 11 a 15 de novembro acontecerá a CBR14 - Campus Party Brasil 2022, no pavilhão de exposições do Anhembi em São Paulo.
A organização da CPBR14 também cedeu à comunidade Debian uma bancada para que possamos marcar nossa presença, e da mesma forma convidamos você a participar da Campus e sentar lá com a gente.

Para mais informações sobre a CPBR14, acesse:
https://brasil.campus-party.org

Em ambos os eventos estaremos presentes para interagir com os(as) participantes, divulgar o Debian, mostrar como qualquer pessoa pode contribuir para o Projeto Debian.  Provavelmente teremos adesivos e camisetas do Debian para vender.
Vamos submeter alguns palestras sobre o Debian para os dois eventos e se forem aceitas, estaremos na programação oficial.

Acompanhe o nosso perfil no twitter mais mais novidades sobre a nossa participação nos dois eventos:
https://twitter.com/debianbrasil

## Desenvolvedores(as) Debian agora podem criar novas tags para o Debtags 

O Grupo Debian foi adicionado[1] ao repositório git para o vocabulário de tags do Debtags[2], e qualquer desenvolvedor(a) Debian agora pode criar novas tags para o Debtags. Pessoas que não são desenvolvedores(as) Debian são bem-vindas para enviar merge requests, porque qualquer desenvolvedor(a) Debian agora pode aprová-lo.

O envio para o master nesse repositório git aciona um job de CI rápido que verifica a consistência do vocabulário. Se passar pelo CI, o novo vocabulário de tag é automaticamente implantado no debtags.debian.org e se torna imediatamente visível no site. 

- [1] https://lists.debian.org/debian-devel/2021/04/msg00059.html
- [2] https://salsa.debian.org/debtags-team/debtags-vocabulary

Autor: Enrico Zini
Texto original: https://lists.debian.org/debian-devel-announce/2021/04/msg00005.html

## screenshots.debian.net remodelado

- Data de envio: 29/04/2021

O site web que coleta capturas de tela (screenshots)[1] de aplicativos empacotados pelo Debian recebeu uma revisão. Ele tem fornecido o backend de imagens para vários gerenciadores de pacotes e para o packages.debian.org[2] por mais de 10 anos. Qualquer pessoa pode fazer upload de novas capturas de tela anonimamente, que depois são aprovadas manualmente. Se você tem uma conta no Salsa[3], você pode agora fazer login no screenshots.debian.net[4] como moderador(a). Você pode publicar diretamente suas próprias capturas de tela, aprovar uploads contribuídos anonimamente e ocultar capturas de tela que você considera inadequadas de outras pessoas.

O código fonte[5] da aplicação web também é mantido no Salsa para outros(as) Debianistas. Também há uma lista de (atualmente 77) problemas em aberto com solicitações de ajustes e sugestões de melhorias. Se você tiver uma sugestão, sinta-se à vontade para abrir uma requisão lá.

Se você gosta de números: hoje temos 11.396 capturas de tela online. E você sabia que a ideia do site veio de um administrador de sistema do Windows de uma grande empresa dando seus primeiros passos com o Ubuntu? 

- [1] https://screenshots.debian.net
- [2] https://packages.debian.org/
- [3] https://salsa.debian.org/
- [4] https://screenshots.debian.net/users/sign_in
- [5] https://salsa.debian.org/debian/debshots

Autor: Christoph Haas
Texto original: https://lists.debian.org/debian-devel-announce/2021/04/msg00005.html

## Chamada para ideias de design do site da DebConf21

- Data de envio: 26/04/2021

Todos vocês jogaram seu chapéu no ringue com o concurso de design DebConf20 no ano passado. Em vez de desperdiçar o tempo das pessoas com um concurso novamente este ano, gostaríamos de fazer um esforço colaborativo para o design da DebConf21. 

Para começar, já temos um site ativo com um design derivado da DebConf20. Mas tenho certeza de que vocês podem fazer melhor coletivamente!
https://debconf21.debconf.org

Aqui está uma tarefa aberta no Salsa onde podemos colaborar no design. Sinta-se à vontade para fazer MRs no repositório on-line da DC21 com ideais/sugestões de design.
https://salsa.debian.org/debconf-team/public/data/dc21-online/-/issues/7

Eu (stefanor@debian.org, tumbleweed no IRC) posso aconselhar sobre o que é e o que não é fácil para fazer no design do site. Idealmente, gostaria que alguém se oferecesse para coordenar o processo de design.

A discussão pode continuar na lista de discussão debconf-team e na tarefa aberta no Salsa.
Lista debconf-team: https://lists.debian.org/debconf-team/

--
Stefano Rivera

Observações:
1) Caso você queira tirar dúvidas em portuguẽs, pode enviar uma mensagem pra mim (Paulo) em privado.

2) Mensagem original em inglês disponível em:
https://lists.debian.org/debconf-team/2021/04/msg00020.html

## Eleição do Líder do Projeto Debian 2021, Jonathan Cartão reeleito.

- Data de envio: 18/04/2021

O período de votação e contagem de votos para a eleição do Líder do Projeto Debian acabou de ser concluída, e o vencedor é Jonathan Carter!!
https://bits.debian.org/2021/04/2021dpl-election.html

Versão completa em pt-br:
https://bits.debian.org/2021/04/2021dpl-election-pt-BR.html

## DebConf21 acontecerá online

- Data de envio: 05/04/2021

Devido a pandemia de COVID-19, a DebConf deste ano será mais uma vez realizada online. As datas exatas ainda não foram definidas, e serão anunciadas posteriormente. No entanto, você pode esperar que a DebConf21 acontecerá durante o terceiro trimestre de 2021, ou seja, aquele período do ano tradicionalmente conhecido como "tempo de DebConf". A data da DebConf21 foi definida semana passada, e será de 22 a 29 de agosto.

Precisamos de uma equipe para fazer a DebConf21 acontecer. No ano passado construímos uma infraestrutura básica para DebConfs online, mas certamente podemos fazer mais este ano. Se você quer tornar a DebConf especial, ou apenas quer ajudar a fazê-la acontecer, por favor, seja voluntário(a) e proponha suas idéias para a lista de discussão debconf-team@lists.debian.org.

Entre outras coisas, precisamos de voluntários(as) para:

- Design: logotipo, site web, camisetas.
- Equipe de conteúdo: seleção e programação das atividades, coordenação dos(as) palestrantes.
- Equipe de patrocínio: contato com potenciais patrocinadores(as).
- Equipe de vídeo: executar as sessões de videoconferência e mixar as transmissões ao vivo.
- Geral: fazer o evento acontecer, coordenando com as outras equipes da DebConf.

Fique ligado(a) para mais notícias sobre a DebConf21 a medida que elas forem sendo anunciadas, e para informações sobre a DebConf22+ asssim que as decisões forem tomadas.

Antonio, em nome da equipe DebConf

Obs: para participar da organização da DebConf é necessário saber se comunicar (ler e escrever) em inglês. Esse conhecimento mínimo é importante porque as mensagens entre os(as) organizadores(as) são trocadas em inglês. Mas não se preocupe, você não precisa ser proficiente em inglês :-)
Contatos

Com a colaboração do Francisco Vilmar, Felipe Maia e Leandro Cunha, que fizeram esse levantamento, listamos abaixo os meios de comunicação da organização da DebConf.

1) Listas de discussão:

Organização:

- https://lists.debian.org/debconf-team
- debconf-team@lists.debian.org

Geral:

- https://lists.debian.org/debconf-discuss
- debconf-discuss@lists.debian.org

2) IRC - rede OFTC

Organização:

- Canal #debconf-team

Geral:

- Canal #debconf

3) Rede Matrix:

- Canal #debconf-team no matrix.debian.social

Observações:
- As salas no IRC e no Matrix estão interconectadas por ponte (bridged), o que significa que as conversas são replicadas automaticamente de uma para a outra (nas duas direções).
- A sala do Matrix fica na instância oficial do Debian (matrix.debian.social) e NÃO em matrix.org.

## Resultado do sprint DDTP-DDTSS da equipe l10n-pt-BR

- Data de envio: 12/04/2021

A equipe de tradução do Debian para o português do Brasil[1] realizou de 20 a 28 de março de 2021 um sprint para traduzir as descrições de pacotes do Debian e encorajar a participação de novos(as) colaboradores(as). Este é o segundo ano consecutivo em que um sprint foi realizado pela equipe de tradução pt-BR. O primeiro[2] aconteceu em maio de 2020 quando a equipe focou na tradução dos arquivos do site debian.org e resultou na tradução/atualização de todas as páginas principais durante os meses seguintes.

A primeira atividade do grupo foi um encontro virtual pelo Jitsi para uma rápida apresentação da equipe l10n-pt-BR . Depois  os veteranos apresentaram aos(às) novatos(as) o DDTP - Projeto de Tradução de Descrições Debian[4] e mostraram na prática como utilizar o DDTSS - Debian Distributed Translation Server Satellite[5]. Durante a semana ainda foram realizados mais dois encontros com os mesmo objetivos para dar oportunidade a quem não pôde participar do encontro anterior.

Durante os dias deste sprint foram realizados mais de 100 novas traduções e mais de 650 revisões de traduções por 15 pessoas, para as quais deixamos nosso muito obrigado por suas contribuições e listamos seus nomes abaixo:

- Alexandro Souza
- Carlos Henrique Lima Melara
- Daniel Lenharo de Souza
- Felipe Viggiano
- Fred Maranhão
- Gabriel Thiago H. dos Santos
- Luis Paulo Linares
- Michel Recondo
- Paulo Henrique de Lima Santana
- Ricardo Berlim Fonseca
- Thiago Hauck
- Thiago Pezzo
- Tiago Zaniquelli
- Victor Moreira
- Wellington Almeida

Para finalizar o sprint, realizamos um último encontro virtual para avaliação e discussão sobre os encaminhamentos[6] futuros.

Nunca é tarde para começar a contribuir com o Debian! Se você também quer ajudar com tradução, leia a nossa página[7] e venha fazer parte da nossa equipe.

- [1] https://wiki.debian.org/Brasil/Traduzir
- [2] https://wiki.debian.org/Brasil/Traduzir/Campanhas/SprintDDTP
- [3] https://wiki.debian.org/Sprints/2020/l10n-portuguese
- [4] https://www.debian.org/international/l10n/ddtp
- [5] https://ddtp.debian.org/ddtss/index.cgi/pt_BR
- [6] https://wiki.debian.org/Brasil/Traduzir/Campanhas/SprintDDTP#Encaminhamentos
- [7] https://wiki.debian.org/Brasil/Traduzir